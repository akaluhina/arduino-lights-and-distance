#include <Wire.h>
#include <LiquidCrystal_I2C.h> //First instal the library: https://github.com/fdebrabander/Arduino-LiquidCrystal-I2C-library
// Set the LCD address.
//if you don't know what is your lcd address.
//just upload lcd I2C addressFinder code and check the serial moniter.
LiquidCrystal_I2C lcd(0x27, 16, 2);
#define t1  20
const int trig = 2;
const int echo = 3; 
int duration = 0;
int distance = 0;
void setup()
{
  //ultrasonic sensor
  pinMode(trig , OUTPUT);
  pinMode(echo , INPUT);
  //LEDS
  for (int i = 4; i <= 13; i++) {
    pinMode(i, OUTPUT);
  }
  //LCD display
  lcd.begin();
  lcd.backlight();
  Serial.begin(9600);
}
void loop()
{
  digitalWrite(trig , HIGH);
  delayMicroseconds(1000);
  digitalWrite(trig , LOW);
  duration = pulseIn(echo , HIGH);
  distance = (duration / 2) / 29.1 ;
  lcd.clear();
  lcd.setCursor(0,0);
  lcd.print("Distance: "); 
  lcd.print(distance);
  if (distance <= 30) {
    lcd.setCursor(0,1);
    lcd.print("Distance is < 30");
    delay(100);
  }
  else{
    lcd.setCursor(0,1);
    lcd.print("Distance is > 30");
    delay(100);
  }
}
